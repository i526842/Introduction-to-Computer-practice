#include <iostream>
using namespace std;

void arrayAdd(int ARR[3][3], int num)
{
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            ARR[i][j]+=num;
        }
    }
    return;
}
int main(void)
{
    int ARR[3][3], num, sum=0;
    cin >> num;
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            cin >> ARR[i][j];
        }
    }
    arrayAdd(ARR, num);
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            sum+=ARR[i][j];
        }
    }
    cout << sum << endl;
    return 0;
}
