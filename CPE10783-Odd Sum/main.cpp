#include <iostream>

using namespace std;

int main()
{
    int cases=0, i=0, j=0, sum=0, data[100][2]={};
    cin >> cases;
    for(i=0;i<cases;i++)
    {
        for(j=0;j<2;j++)
        {
            cin >> data[i][j];
        }
    }
    for(i=0;i<cases;i++)
    {
        cout << "Case "<< i+1<< ": ";
        for(j=data[i][0];j<data[i][1]+1;j++)
        {
            if(j%2==1)
            {
                sum+=j;
            }
        }
        cout << sum<< endl;
        sum=0;
    }
    return 0;
}
