#include <iostream>
#include <cstring>
using namespace std;

// ------ ����϶� ------
class Person
{
    private:
        int id;
        string name;
    public:
        Person(int i, string n): id(i), name(n) {}
        int get_id()
        {
            return id;
        }
        string get_name()
        {
            return name;
        }
};

class Grade: public Person
{
    private:
        int math, english;
    public:
        Grade(int i, string n): Person(i, n) {}
        void set(int m, int e)
        {
            math=m; english=e;
        }
        void show()
        {
            cout << "Person "<< get_id()<< "'s grade"<< endl;
            cout << "Name: "<< get_name()<< endl;
            cout << "Math: "<< math<< endl;
            cout << "English: "<< english<< endl;
            cout << "Average: "<< (float)(math+english)/2<< endl;
        }
};
// ---------------------

int main()
{
    Grade David(1359, "David"), Mary(2458, "Mary");
    David.set(79, 23);
    David.show();
    Mary.set(100, 90);
    Mary.show();
    return 0;
}
