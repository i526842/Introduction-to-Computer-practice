#include <iostream>

using namespace std;

class CBox{
    public:
        int length;
        int width;
        int height;

        int volume(void){
            return length*width*height;
        }

        int surfaceArea(void){
            return 2*(length*width+length*height+width*height);
        }
};
int main(void)
{
    CBox a;
    a.length=10;
    a.width=12;
    a.height=13;
    cout << "長 : "<< a.length<< endl;
    cout << "寬 : "<< a.width<< endl;
    cout << "高 : "<< a.height<< endl;
    cout << "體積 : "<< a.volume()<< endl;
    cout << "表面積 : "<< a.surfaceArea();
    return 0;
}
