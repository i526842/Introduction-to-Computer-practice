#include <iostream>
using namespace std;

struct airbag {
    float length;
    float width;
    float height;
};

void expan(struct airbag *T, float c)
{
    T->length=T->length*c;
    T->width=T->width*c;
    T->height=T->height*c;
}

int main(void)
{
    float coe = 1.12;
    struct airbag Taiwan = {10.3, 14.3, 16.6};
    cout << "----------"<< endl;
    cout << "Origin value"<< endl;
    cout << "----------"<< endl;
    cout << "Length: "<< Taiwan.length<< endl;
    cout << "Width: "<< Taiwan.width<< endl;
    cout << "Height: "<< Taiwan.height<< endl;
    cout << "Volume: "<< (Taiwan.length)*(Taiwan.width)*(Taiwan.height)<< endl;
    expan(&Taiwan, coe);
    cout << "----------"<< endl;
    cout << "Value in space"<< endl;
    cout << "----------"<< endl;
    cout << "Length: "<< Taiwan.length<< endl;
    cout << "Width: "<< Taiwan.width<< endl;
    cout << "Height: "<< Taiwan.height<< endl;
    cout << "Volume: "<< (Taiwan.length)*(Taiwan.width)*(Taiwan.height)<< endl;
    return 0;
}
