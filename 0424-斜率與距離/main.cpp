#include <iostream>
#include <cmath>
using namespace std;

class Point
{
  public:
    int x, y;
    Point(int a, int b) { x = a; y = b; }
    Point operator+(Point &p)
    {
        cout << "distance: "<< sqrt(pow((p.x-x),2)+pow((p.y-y),2))<< endl;
    }
    Point operator/(Point &p)
    {
        cout << "slope: "<< (float)(p.y-y)/(float)(p.x-x)<< endl;
    }
};

int main(void)
{
  Point p1(0,3);
  Point p2(3,5);
  p1+p2;
  p1/p2;
  return 0;
}
