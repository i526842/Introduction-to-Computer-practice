#include <iostream>
#include <stdio.h>
#include <math.h>
using namespace std;

class Student
{
  private:
    double tall, weight, BMI;

  public:
    Student()
    {
        tall = 0;
        weight = 0;
        BMI = 0;
    }
    void set()
    {
        cin >> tall>> weight;
        BMI=weight/pow((tall/100),2);
    }
    void show(int n)
    {
        cout << "Student "<< n<< ":"<< endl;
        cout << "tall: "<< tall<< "cm"<< endl;
        cout << "weight: "<< weight<< "kg"<< endl;
        cout << "BMI: "<< BMI<< endl;
    }
};

int main()
{
    int n, i=0;
    cin >> n;
    Student *ptr=new Student[n];
    for(i=0;i<n;i++)
    {
        ptr[i].set();
    }
    for(i=0;i<n;i++)
    {
        ptr[i].show(i+1);
    }
    delete [] ptr;
    return 0;
}
