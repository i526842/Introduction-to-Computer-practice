#include <iostream>
#include <fstream>
using namespace std;

int main()
{
    int a[4][6], b[6][4], c[4][4], i=0, j=0, k=0;
    fstream matrixA, matrixB, matrixC;
    matrixA.open("A.txt", ios::in);
    matrixB.open("B.txt", ios::in);
    matrixC.open("C.txt", ios::out);
    for(i=0;i<4;i++)
    {
        for(j=0;j<6;j++)
        {
            matrixA >> a[i][j];
        }
    }
    for(i=0;i<6;i++)
    {
        for(j=0;j<4;j++)
        {
            matrixB >> b[i][j];
        }
    }
    for(i=0;i<4;i++)
    {
        for(j=0;j<4;j++)
        {
            c[i][j]=0;
            for(k=0;k<6;k++)
            {
                c[i][j]+=a[i][k]*b[k][j];
            }
            matrixC << c[i][j]<< " ";
        }
        matrixC << endl;
    }
    matrixA.close();
    matrixB.close();
    matrixC.close();
    return 0;
}
