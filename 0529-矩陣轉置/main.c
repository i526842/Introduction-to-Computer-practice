#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a[10][10], b[10][10], c=1, m, n, i=0, j=0;
    while(scanf("%d %d", &m, &n) != EOF) {
        for(i=0;i<m;i++) {
            for(j=0;j<n;j++) {
                    scanf("%d", &a[i][j]);
            }
        }
        printf("Matrix %d\n", c);
        c+=1;
        for(i=0;i<n;i++) {
            for(j=0;j<m;j++) {
                b[i][j] = a[j][i];
                printf("%d ", b[i][j]);
            }
            printf("\n");
        }
    }
    return 0;
}
