﻿#include <iostream>
#include <cstring>

using namespace std;

class Account
{
private:
	string name;
	int money;
public:
	Account(string st)
	{
		name = st ;
		money = 2000;
	}
	Account(string st, int m)
	{
		name = st;
		money = m;
	}
	void outward(int num, Account &obj)
	{
		obj.inward(num);
		money -= num;
		cout << name << " transfers " << num << " dollors to " << obj.name << "." << endl;
	}
	void inward(int num)
	{
		money += num;
	}
	void show()
	{
		cout << name << " has " << money << " dollors." << endl;
	}
};

int main(void)
{
	// 自行測試
	return 0;
}