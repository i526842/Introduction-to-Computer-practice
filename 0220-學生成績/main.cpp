#include <iostream>
using namespace std;

struct person {
    char name[20];
    int math;
    int english;
    int chinese;
};

int main()
{
    struct person student[5] = {
        {"q", 50, 60, 70},
        {"w", 40, 30, 20},
        {"e", 80, 90, 70},
        {"r", 70, 40, 40},
        {"t", 10, 20, 30}
    };
    for(int i=0; i<5; i++)
    {
        cout << "Person: "<< i+1<< endl;
        cout << "Name: "<< student[i].name<< endl;
        cout << "Chinese: "<< student[i].chinese<< endl;
        cout << "English: "<< student[i].english<< endl;
        cout << "Math: "<< student[i].math<< endl;
        cout << "Average: "<< (float) (student[i].chinese+student[i].english+student[i].math)/3<< endl;
        cout << "---------------------"<< endl;
    }
    return 0;
}
