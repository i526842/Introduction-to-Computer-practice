#include <iostream>
using namespace std;

class Person
{
    private:
        string name;
        int money;
    public:
        Person(string n="", int m=0)
        {
            name=n; money=m;
        }
        Person compare(Person &obj)
        {
           if(this->money>obj.money)
           {
               return *this;
           }
           else
           {
               return obj;
           }
        }
        void show()
        {
            cout << "Name: "<< name<< endl;
            cout << "Money: "<< money<< endl;
        }
};

int main(void)
{
    Person men[3] = {
        Person("P1",100000),
        Person("P2",35000),
        Person("P3",990000)
    };
    for(int i=0;i<3;i++)
    {
        men[i].show();
    }
    Person max_Person;
    max_Person = ((men[0].compare(men[1])).compare(men[2]));
    cout << "------Max------"<< endl;
    max_Person.show();
    return 0;
}
