#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a, b, c;
    char s;
    while(scanf("%d%c%d", &a, &s, &b) !=EOF)
    {
        switch(s)
        {
            case '+':
                c=a+b;
                printf("%d+%d=%d\n", a, b, c);
                break;
            case '-':
                c=a-b;
                printf("%d-%d=%d\n", a, b, c);
                break;
            case '*':
                c=a*b;
                printf("%d*%d=%d\n", a, b, c);
                break;
            case '/':
                printf("%d/%d=%.3f\n", a, b, (float)a/b);
                break;
            default:
                puts("error");
                break;
        }
    }
    return 0;
}
