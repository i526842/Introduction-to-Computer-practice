#include <iostream>
#include <cstring>
using namespace std;

// ---------- ����϶� ----------
class Person
{
    private:
        int money;
        int age;
        int id;
        string name;
    public:
        Person(string n)
        {
            money=0;
            age=0;
            id=0;
            name=n;
        }
        Person(string n, int i)
        {
            money=0;
            age=0;
            id=i;
            name=n;
        }
        Person(string n, int i, int m)
        {
            money=m;
            age=0;
            id=i;
            name=n;
        }
        Person( string n, int i, int m, int a)
        {
            money=m;
            age=a;
            id=i;
            name=n;
        }
        void show()
        {
            cout << "name: "<< name<< endl;
            cout << "id: "<< id<< endl;
            cout << "money: "<< money<< endl;
            cout << "age: "<< age<< endl;
        }
};
// -------------------------------

int main(void)
{
    Person p1(?), p2(?, ?), p3(?, ?, ?), p4(?, ?, ?, ?);
    p1.show();
    cout << "------------" << endl;
    p4.show();
    return 0;
}
