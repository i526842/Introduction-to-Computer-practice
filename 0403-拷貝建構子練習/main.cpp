#include <iostream>
using namespace std;

class Complex
{
    private:
        int Real;
        int Image;
    public:
        Complex()
        {
            Real = 1;
            Image = 24;
        }
        // ------- ����϶� -------
        Complex(const Complex &obj)
        {
            Real=2*(obj.Real);
            Image=4*(obj.Image);
        }
        // -----------------------
        void show()
        {
            cout << Real << " + " << Image << "i" << endl;
        }
};

int main()
{
    Complex c1;
    Complex c2(c1);
    Complex c3(c2);
    cout << "c1: ";
    c1.show();
    cout << "c2: ";
    c2.show();
    cout << "c3: ";
    c3.show();
    return 0;
}
