class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {//注意vector的相關使用
        for(int i=0;i<nums.size();i++)
        {
            for(int j=i+1;j<nums.size();j++)
            {
                if(nums.at(i)+nums.at(j)==target)
                {
                    return {i,j};
                }
            }
        }
        return nums;
    }
};
