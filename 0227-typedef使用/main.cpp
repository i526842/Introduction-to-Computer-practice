#include <iostream>
#include <cmath>
using namespace std;

typedef struct {
    double x;
    double y;
} ;

int main()
{
    double square;
    __ p1, p2;
    p1.x = 0;
    p1.y = 0;
    p2.x = 3;
    p2.y = 5;
    // ------ ����϶� ------
    cout << "p1 = (" << p1.x << " , " << p1.y << ")" << endl;
    cout << "p2 = (" << p2.x << " , " << p2.y << ")" << endl;
    square = (p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y);
    cout << "distance(p1, p2) = " << sqrt(square) << endl;
    // ----------------------
    return 0;
}
