#include <iostream>
#include <cstring>
using namespace std;

// ------ ����϶� ------
class Character_base
{
    public:
        string name;
        int age;
        Character_base(string n, int a): name(n), age(a) {}
        void action()
        {
            cout << "Name: "<< name<< endl;
            cout << "Age: "<< age<< endl;
            cout << "Action: "<< "Walk"<< endl;
        }
};

class Swordman: public Character_base
{
    public:
        string career = "swordman";
        Swordman(string n, int a): Character_base(n, a) {}
        void action()
        {
            cout << "Name: "<< name<< endl;
            cout << "Age: "<< age<< endl;
            cout << "Career: "<< career<< endl;
            cout << "Action: "<< "Wield the sword"<< endl;
        }
};

class Wizard: public Character_base
{
    public:
        string career = "wizard";
        Wizard(string n, int a): Character_base(n, a) {}
        void action()
        {
            cout << "Name: "<< name<< endl;
            cout << "Age: "<< age<< endl;
            cout << "Career: "<< career<< endl;
            cout << "Action: "<< "Magic"<< endl;
        }
};

// -------------------

int main(void)
{

    return 0;
}
