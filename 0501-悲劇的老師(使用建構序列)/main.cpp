#include <iostream>
using namespace std;

// 這個類別不再做更改
class Grade
{
  public:
    int math, chinese;
    Grade(int m = 0, int c = 0) : math(m), chinese(c) {}
};

// ---------- 提交區塊 ----------
class NewGrade: public Grade
{
  public:
    int english;
    NewGrade(int m, int c, int e) : Grade(m,c)
    {
        math=m;
        chinese=c;
        english=e;
    }
    void show()
    {
        cout << "math: "<< math<< endl;
        cout << "chinese: "<< chinese<< endl;
        cout << "english: "<< english<< endl;
        cout << "average: "<< (float)(math+chinese+english)/3<< endl;
    }
};
// ----------------------------

int main()
{
    // 自行測試
    return 0;
}
