#include <iostream>
using namespace std;

class Person
{
    public:
        string name;
        int Money;
        static int money;
        Person(string n="", int m=0)
        {
            name=n; Money=m;
        }
        void inward(int m)
        {
            Money+=m;
            cout << name<< " deposits $"<< m<< endl;
        }
        void outward(int m)
        {
            Money-=m;
            cout << name<< " withdraws $"<< m<< endl;
        }
        void show(Person i,Person j,Person k)
        {
            money=this->Money+i.Money+j.Money+k.Money;
            cout << "They have $"<< money<< endl;
        }
};
int Person::money=0;
int main()
{
    Person roommate[4]=
    {
        Person("P1",3600),
        Person("P2",7287),
        Person("P3",6433),
        Person("P4",50)
    };
    cout << "P1 deposits $3600"<< endl;
    cout << "P2 deposits $7287"<< endl;
    cout << "P3 deposits $6433"<< endl;
    cout << "P4 deposits $50"<< endl;
    cout << "--------------" << endl;
    roommate[0].show(roommate[1],roommate[2],roommate[3]);
    cout << "--------------" << endl;
    roommate[2].inward(1787);
    roommate[1].outward(7777);
    roommate[3].inward(6009);
    roommate[1].outward(77);
    roommate[3].inward(79990);
    roommate[0].outward(77);
    roommate[1].outward(537);
    roommate[0].inward(42000);
    roommate[2].inward(30);
    roommate[0].inward(34);
    roommate[1].outward(1);
    cout << "--------------" << endl;
    roommate[0].show(roommate[1],roommate[2],roommate[3]);
    return 0;
}
