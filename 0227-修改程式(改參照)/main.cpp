#include <iostream>
using namespace std;

int *min(int *, int *);

int main(void)
{
    int a = 32, b = 59;
    cout << "a = " << a << ", b = " << b << endl;
    min(a, b) = 100;

    cout << "a = " << a << ", b = " << b << endl;
    return 0;
}

int &min(int &x, int &y)
{
    if (x > y)
        return y;
    else
        return x;
}
