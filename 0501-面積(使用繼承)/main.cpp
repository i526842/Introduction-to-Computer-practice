#include <iostream>
using namespace std;

class CShape
{
    public:
    int width, height;
};

class CRectangle: public CShape
{
    public:
    CRectangle(int w,int h)
    {
        width=w;
        height=h;
    }
    void area()
    {
        cout << "Rectangle"<< endl;
        cout << "width: "<< width<< endl;
        cout << "height: "<< height<< endl;
        cout << "area: "<< width*height<< endl;
    }
};

class CTriangle: public CShape
{
    public:
    CTriangle(int w,int h)
    {
        width=w;
        height=h;
    }
    void area()
    {
        cout << "Triangle"<< endl;
        cout << "base: "<< width<< endl;
        cout << "height: "<< height<< endl;
        cout << "area: "<< (float)(width*height)/2<< endl;
    }
};

int main()
{
    return 0;
}
