#include <iostream>
using namespace std;

// ---------- 提交區塊 ----------
class Matrix
{
    public:
        int matrix[3][3];
        Matrix(int m[3][3])
        {
            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                    {
                        matrix[i][j]=m[i][j];
                    }
            }
        }

        void show()
        {
            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                    {
                        cout << matrix[i][j]<< " ";
                    }
                cout << endl;
            }
        }

        void operator+(Matrix &m)
        {
            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                    {
                        cout << matrix[i][j]+m.matrix[i][j]<< " ";
                    }
                cout << endl;
            }
        }

        void operator-(Matrix &m)
        {
            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                    {
                        cout << matrix[i][j]-m.matrix[i][j]<< " ";
                    }
                cout << endl;
            }
        }
};
// ----------------------------

int main(void)
{
    // 矩陣內的值自訂
    int a[3][3] = {1,2,3,
                   4,5,6,
                   7,8,9};
    int b[3][3] = {0,1,2,
                   3,4,5,
                   6,7,8};
    Matrix m1(a), m2(b);
    cout << "m1: " << endl;
    m1.show();
    cout << "m2: " << endl;
    m2.show();
    cout << "m1+m2: " << endl;
    m1+m2;
    cout << "m1-m2: " << endl;
    m1-m2;
    return 0;
}
            // 直接將結果在此cout 出來
