#include <iostream>
#include <math.h>
#include <cstdlib>

using namespace std;

int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        cout << "Arg(s) error!" << endl;
        return -1;
    }
    double x=atof(argv[1]);
    int n=atoi(argv[2]);
    cout << pow(x,n) << endl;
    return 0;
}
