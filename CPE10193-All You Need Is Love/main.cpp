#include <iostream>

using namespace std;
int transfer(char a[31])
{
    int b=0;
    for(int i=0;i<30;i++)
    {
        if (a[i]=='\0')
        {
            break;
        }
        b=b*2;
        if (a[i]=='1')
        {
            b=b+1;
        }
    }
    return b;
}
int GCD(int a, int b)
{
    while (a!=0&&b!=0)
    {
        if(a<b)
        {
            int temp=a;
            a=b;
            b=temp;
        }
        a=a%b;
    }
    if(a>=b)
    {
        return a;
    }
    else
    {
        return b;
    }
}
int main()
{
    int i=0, cases=0, data[10000][2];
    char a[31], b[31];
    cin >> cases;
    for(i=0;i<cases;i++)
    {
        cin >> a>> b;
        int a0=transfer(a);
        int b0=transfer(b);
        data[i][0]=a0;
        data[i][1]=b0;
        a0=0;
        b0=0;
    }
    for(i=0;i<cases;i++)
    {
        if(GCD((data[i][0]),(data[i][1]))==1)
        {
            cout << "Pair #"<< i+1<< ": Love is not all you need!"<< endl;
        }
        else
        {
            cout << "Pair #"<< i+1<< ": All you need is love!"<< endl;
        }
    }
    return 0;
}
