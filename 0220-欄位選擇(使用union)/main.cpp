#include <iostream>
using namespace std;

union data
{
    int number;
    char word;
};

int main()
{
    union data member;
    member.number = 9527;
    cout << "id: " << member.number << endl;
    // 印出member的記憶體大小
    cout << "----------------------" << endl;
    member.word = 'F';
    // 印出member的記憶體大小
    return 0;
}
