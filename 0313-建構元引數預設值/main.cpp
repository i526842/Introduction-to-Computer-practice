#include <iostream>

using namespace std;

// ---------- 提交區塊 ----------
class CRectangle
{
    private:
        int width;
        int height;
    public:
        CRectangle()
        {
            width=10;
            height=10;
        }
        CRectangle(int w, int h)
        {
            width=w;
            height=h;
        }
        void show()
        {
            cout << "width: " << width << endl;
            cout << "height: " << height << endl;
        }
};
// ------------------------------

int main(void)
{
    // 自行使用show()進行測試
    return 0;
}
